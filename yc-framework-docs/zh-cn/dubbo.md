# Dubbo

只需引入如下依赖，按照官方文档配置即可:
```
<dependency>
    <groupId>com.yc.framework</groupId>
    <artifactId>yc-common-dubbo</artifactId>
</dependency>

```


# YC-Framework如何使用Dubbo
[轻松搭建Dubbo架构体系](https://mp.weixin.qq.com/s?__biz=MzUxODk0ODQ3Ng==&mid=2247486743&idx=1&sn=9a9c4a6c998b4a9bfa8d14ecac742fd3&chksm=f9805e04cef7d7126b8d9ec88fcf2e723acf1d431bb67ab6aab5abc98dbfc4a480fe41e9d8c5&token=730841189&lang=zh_CN#rd)