# Flink

Apache Flink 官网:
https://flink.apache.org/

Apache Flink 下载:
https://flink.apache.org/downloads.html

Apache Flink 源代码:
https://github.com/apache/flink

如果你需要用到Flink，只需引入如下依赖即可:
```
<dependency>
    <groupId>com.yc.framework</groupId>
    <artifactId>yc-common-flink</artifactId>
</dependency>

```

# YC-Framework如何使用Flink
[从零开始学YC-Framework之Flink](https://youcongtech.com/2022/10/06/%E4%BB%8E%E9%9B%B6%E5%BC%80%E5%A7%8B%E5%AD%A6YC-Framework%E4%B9%8BFlink/)